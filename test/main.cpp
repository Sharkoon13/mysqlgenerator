
#include <gtest/gtest.h>

int main(int argc, char **argv) {
    setlocale(LC_CTYPE, "");

    ::testing::InitGoogleTest(&argc, argv);

    int ret = RUN_ALL_TESTS();

    return ret;
}

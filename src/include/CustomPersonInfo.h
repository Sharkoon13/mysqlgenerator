//
// Created by iddqd13 on 3/14/18.
//

#ifndef AGENTCASTLEGENERATOR_CUSTOMPERSONINFO_H
#define AGENTCASTLEGENERATOR_CUSTOMPERSONINFO_H

#include <DbConnection.h>
#include <cstring>
#include <vector>
#include <DeviceSdk/ILogger.h>


namespace Castle {

    struct CustomPersonInfo {
        uint32_t id;

        std::string name;

        uint64_t codekey;

        CustomPersonInfo(uint32_t _id, std::string _name, uint64_t _codekey) : id{_id}, name{std::move(_name)},
                                                                               codekey{_codekey} {}

        CustomPersonInfo() = delete;

        CustomPersonInfo(const CustomPersonInfo &other) = default;
    };

    std::vector<CustomPersonInfo>
    retriveFromDb(const unsigned int count, MySQL::DbConnectionPtr baseConnectionPtr, DeviceSdk::ILogger *logger);

}

#endif //AGENTCASTLEGENERATOR_CUSTOMPERSONINFO_H

//
// Created by iddqd13 on 3/13/18.
//

#ifndef AGENTCASTLEGENERATOR_DBCONNECTION_H
#define AGENTCASTLEGENERATOR_DBCONNECTION_H

#include <memory>
#include <type_traits>

#include <cppconn/connection.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
#include <unordered_set>


namespace Castle {
namespace MySQL {

    class DbConnection;

    inline namespace Ptr {

        typedef std::unique_ptr<DbConnection> DbConnectionPtr;

        typedef std::unique_ptr<sql::ResultSet> ResultSetPtr;
        typedef std::unique_ptr<sql::Connection> ConnectionPtr;
        typedef std::unique_ptr<sql::Statement> StatementPtr;
        typedef std::unique_ptr<sql::PreparedStatement> PreparedStatementPtr;

        template<typename T>
        std::unique_ptr<T> WrapUniq(T *ptr) {
            return std::unique_ptr<T>(ptr);
        }
    }

    /**  @class DbConnection
      *  @brief
      */
    class DbConnection {
    public:
        explicit DbConnection(const char *dbUri);

        const std::string uri() const { return m_uri; }

        Ptr::DbConnectionPtr clone() const;

        Ptr::StatementPtr createStatement();

        Ptr::PreparedStatementPtr prepareStatement(const sql::SQLString &sql);


    private:
        const std::string m_uri;
        Ptr::ConnectionPtr m_connection;
    };

    // Custom Hash Functor that will compute the hash on the
    // passed string objects length
    struct DbConnectionHasher {
        size_t
        operator()(const DbConnection &obj) const {
            return std::hash<std::string>()(obj.uri());
        }
    };

    struct DbConnectionComparator {
        bool
        operator()(const DbConnection &obj1, const DbConnection &obj2) const {
            return obj1.uri() == obj2.uri();
        }
    };

    typedef std::unordered_set<DbConnection, DbConnectionHasher, DbConnectionComparator> DbConnectionSet;

    extern const char LOGS_INSERT_STATEMENT[];
    extern const char ALARMLOG_INSERT_STATEMENT[];


}

}


#endif //AGENTCASTLEGENERATOR_DBCONNECTION_H

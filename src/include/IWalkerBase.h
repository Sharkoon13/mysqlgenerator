//
// Created by iddqd13 on 3/15/18.
//

#ifndef AGENTCASTLEGENERATOR_IWALKERBASE_H
#define AGENTCASTLEGENERATOR_IWALKERBASE_H

#include <DbConnection.h>
#include <cstring>
#include <cstdint>
#include <forward_list>
#include <mutex>
#include <atomic>
#include <thread>
#include <memory>
#include <Helpers/Logger.h>

namespace Castle {

    typedef std::forward_list<std::pair<uint32_t, uint8_t >> AccessPointPath;
    typedef AccessPointPath::iterator AccessPointPathStep;

    class IWalkerBase : public DeviceSdk::LoggerOwner {
    public:
        explicit IWalkerBase(DeviceSdk::ILogger *logger = nullptr) : DeviceSdk::LoggerOwner(logger), m_walking{false} {}

        virtual ~IWalkerBase() {
            stopWalking();
            if (m_thread.joinable()) {
                m_thread.join();
            }
        }

        /**
         * with mutex set variable m_walking to false ( internal thread work while m_waling is true )
         */
        virtual void stopWalking() {
            std::lock_guard<std::mutex> lockGuard(m_mutex);

            m_walking = false;
        }

        /**
         * Start detached thread with walking to zones
         * @param walkingPath forward_list with Access Point Identificators.
         * @param baseConnectionPtr clone of original Db connection
         * !!!!Warning! subthread take ownership of baseConnectionPtr!
         * calling this method must be like : p->startWalking(path , conn->clone() );
         */
        virtual void startWalking(const AccessPointPath &walkingPath, MySQL::DbConnectionPtr baseConnectionPtr) = 0;

    protected:
        AccessPointPath m_path;
        AccessPointPathStep m_currentStep;
        std::mutex m_mutex;
        std::atomic<bool> m_walking;
        std::thread m_thread;
    };


    typedef std::unique_ptr<IWalkerBase> IWalkersPtr;
}
#endif //AGENTCASTLEGENERATOR_IWALKERBASE_H

//
// Created by iddqd13 on 3/14/18.
//

#ifndef AGENTCASTLEGENERATOR_APP_H
#define AGENTCASTLEGENERATOR_APP_H

#include <Helpers/Log.h>
#include <Helpers/Logger.h>
#include <DeviceSdk/Support/ImplementContract.h>
#include <DeviceSdk/ContractCast.h>
#include <forward_list>
#include <vector>
#include <DbConnection.h>
#include <IWalkerBase.h>


namespace Castle {
    class App : protected DeviceSdk::ILoggerOwner {
    public:
        App(const std::forward_list<std::string> &uris, DeviceSdk::LogLevel level = DeviceSdk::LOG_All);

        virtual ~App() {}

        void start(unsigned int countOfWalkers = 1);

        void trace(const char *str);

    protected:
        inline virtual DeviceSdk::ILogger *getLogger() const override {
            return DeviceSdk::contract_cast<DeviceSdk::ILogger>(m_logger.get());
        }

    private:
        std::unique_ptr<DeviceSdk::Logger> m_logger;
        MySQL::DbConnectionSet m_connections;
        std::vector<IWalkersPtr> m_walkers;
    };

}

#endif //AGENTCASTLEGENERATOR_APP_H

//
// Created by iddqd13 on 3/13/18.
//

#ifndef AGENTCASTLEGENERATOR_UTILS_H
#define AGENTCASTLEGENERATOR_UTILS_H

#include <boost/date_time/posix_time/ptime.hpp>

#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>



namespace Castle {
namespace utils {

    struct membuf : std::basic_streambuf<char> {
        membuf(char *begin, char *end) {
            this->setg(begin, begin, end);
        }
    };

    inline std::string currentTime(int yearShift = 0) {
        namespace pt = boost::posix_time;


        //'2013-10-10 15:11:18'
        pt::time_facet *facet = new pt::time_facet("%Y-%m-%d %H:%M:%S");
        std::ostringstream res;
        res.imbue(std::locale(res.getloc(), facet));
        //// TEMPORARY DECREASE ONE YEAR FOR DEBUG
        if (yearShift != 0) {
            if (yearShift < 0) {
                res << (boost::date_time::second_clock<boost::posix_time::ptime>::local_time() -
                        boost::gregorian::years(1));
            } else {
                res << (boost::date_time::second_clock<boost::posix_time::ptime>::local_time() +
                        boost::gregorian::years(1));
            }
        } else {
            res << boost::date_time::second_clock<boost::posix_time::ptime>::local_time();
        }
        return res.str();
    }

    template<typename E>
    constexpr unsigned int toUInt(E e) {
        return static_cast<unsigned int>(static_cast<typename std::underlying_type<E>::type>(e));
    }

    enum class LogsCols : unsigned int {
        LOGTIME = 1,
        AREA,
        LOGDATA,
        EMPHINT,
        DEVHINT,
        FRAMETS
    };

    enum class AlarmLogCols : unsigned int {
        LOGTIME = 1,
        LINEID,
        NEWSTATE,
        FRAMETS
    };

    enum class AlarmLineState : unsigned int {
        NOT_ACTIVE = 1,
        ACTIVE = 2,
        ALARM = 3
    };

    enum class LogDataType : unsigned int {
        passDetected,
        passDeny,
        fireUnlockBegin,
        fireUnlockEnd,
        boxOpened,
        boxClosed,
        apOnlineStatus
    };

    constexpr int NULL_VAL = 0;


    inline std::array<unsigned char, 20>
    toArray(const uint32_t devhint, const uint8_t dir, const uint32_t objId, const uint64_t codekey) {
        std::array<unsigned char, 20> logdataBuff{};
        std::fill(logdataBuff.begin(), logdataBuff.end(), 0);
        logdataBuff[0] = 0xFE;  //constant 0xFE06    //0xfe06
        logdataBuff[1] = 0x06;  //constant 0xFE06
        logdataBuff[2] = (devhint & 0xFF00) >> 2;  //DEVHINT  //0x000b
        logdataBuff[3] = devhint & 0xFF;          //DEVHINT
        logdataBuff[4] = dir;                          //DIRECTION  0x02
        logdataBuff[5] = 0x03;                          //BASE DECISION  0x03
        {
            const unsigned char *begin = reinterpret_cast< const unsigned char * >( std::addressof(objId));
            const unsigned char *end = begin + sizeof(objId);
            unsigned long idx = 6 + sizeof(objId);
            for (auto *it = begin; it != end; ++it) {
                logdataBuff[--idx] = *it;
            }
        }
        {
            const unsigned char *begin = reinterpret_cast< const unsigned char * >( std::addressof(codekey));
            const unsigned char *end = begin + sizeof(codekey);
            unsigned long idx = 10 + sizeof(codekey);
            for (auto *it = begin; it != end; ++it) {
                logdataBuff[--idx] = *it;
            }
        }

        logdataBuff[18] = 0xFF;   //end HEX by 0x FF FF
        logdataBuff[19] = 0xFF;

        return logdataBuff;
    }

    template<uint32_t arrSize>
    inline std::array<unsigned char, arrSize>
    toArray(LogDataType type, const uint32_t devhint, uint8_t addParam = 0) {
        std::array<unsigned char, arrSize> logdataBuff{};
        std::fill(logdataBuff.begin(), logdataBuff.end(), 0);
        logdataBuff[0] = 0xFE;
        logdataBuff[2] = (devhint & 0xFF00) >> 2;  //DEVHINT  //0x000b
        logdataBuff[3] = devhint & 0xFF;          //DEVHINT
        if (type == LogDataType::fireUnlockBegin) {
            logdataBuff[1] = 0x08;

        } else if (type == LogDataType::fireUnlockEnd) {
            logdataBuff[1] = 0x09;
        } else if (type == LogDataType::boxOpened) {
            logdataBuff[1] = 0x0A;
            logdataBuff[4] = 0x01;  //z.B. one box - one controller - one AP
        } else if (type == LogDataType::boxClosed) {
            logdataBuff[1] = 0x0B;
            logdataBuff[4] = 0x01;  //z.B. one box - one controller - one AP
        } else if (type == LogDataType::apOnlineStatus) {
            logdataBuff[1] = 0x0C;
            logdataBuff[4] = addParam;  // 0 -online , 1 - offline
        }
        return logdataBuff;
    };

}


}


#endif //AGENTCASTLEGENERATOR_UTILS_H

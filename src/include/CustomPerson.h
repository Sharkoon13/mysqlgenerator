//
// Created by iddqd13 on 3/13/18.
//

#ifndef AGENTCASTLEGENERATOR_CUSTOMPERSON_H
#define AGENTCASTLEGENERATOR_CUSTOMPERSON_H

#include <IWalkerBase.h>


namespace Castle {

/**  @class CustomPerson
  *  @brief read random person from personal,and walk through
  */
    class CustomPerson : public IWalkerBase {
    public:
        /**
         * construct Custrom Person for walking through zones
         * @param name Person Name
         * @param idx Person Id
         * @param codekey Person unique key (in logs last 4|8 bytes )
         */
        CustomPerson(const std::string &name, uint32_t idx, uint64_t codekey, DeviceSdk::ILogger *logger = nullptr);

        virtual ~CustomPerson() = default;

        /**
         * Start detached thread with walking to zones
         * @param walkingPath forward_list with Access Point Identificators.
         * @param baseConnectionPtr clone of original Db connection
         * !!!!Warning! subthread take ownership of baseConnectionPtr!
         * calling this method must be like : p->startWalking(path , conn->clone() );
         */
        virtual void
        startWalking(const AccessPointPath &walkingPath, MySQL::DbConnectionPtr baseConnectionPtr) override;

    private:
        const std::string m_name;
        const uint32_t m_idx;
        const uint64_t m_codekey;
    };

}


#endif //AGENTCASTLEGENERATOR_CUSTOMPERSON_H

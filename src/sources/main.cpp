//
// Created by iddqd13 on 3/13/18.
//

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <regex>

#include <mysql_driver.h>
#include <mysql_connection.h>

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>

#include <DbConnection.h>

#include <App.h>
#include <boost/asio.hpp>


static void handler(const boost::system::error_code &error, int signal_number) {
//    //std::cout << "handling signal " << signal_number << std::endl;
//    //exit(1);
}

static void wait() {
    boost::asio::io_service io_service;
    boost::asio::signal_set signals(io_service, SIGINT);
    signals.async_wait(handler);
    io_service.run();
}

int
main(int argc, char *argv[]) {

    DeviceSdk::LogLevel level{DeviceSdk::LOG_Debug};

    if (argc < 4) {
        std::cout << "Error. Missing arguments (Loglevel, walkersCount and connects to databases. ) " << std::endl
                  << "For launching applicaiton need specify db connection" << std::endl
                  << "LOG_LEVEL WALKERCOUNT db://username:password@hostIpOrName/mysql/DbName/charset" << std::endl
                  << "for example: " << std::endl
                  << "100 3 db://root:pass@10.20.30.253/mysql/tc-db-main/utf8" << std::endl;
        return 0;
    }

    level = static_cast<DeviceSdk::LogLevel> ( atoi(argv[1]));

    uint32_t walkersCount = static_cast<uint32_t >( atoi(argv[2]));

    std::forward_list<std::string> dbConns;

    for (int i = 3; i < argc; ++i) {
        dbConns.emplace_front(argv[i]);
    }

    auto app = std::make_unique<Castle::App>(dbConns, level);

    app->trace(("Start Generator with log level + " + std::to_string(level) + " and walkers count = " +
                std::to_string(walkersCount)).c_str());
    app->start(walkersCount);

//    std::this_thread::sleep_for(std::chrono::minutes(1));
    wait();

    return 0;
}

//int main(int argc, char *argv[]) {
//    {
//        if (argc < 2) {
//            std::cout << "Error. Missing arguments ( connect to database. ) " << std::endl
//                      << "For launching applicaiton need specify db connection" << std::endl
//                      << "db://username:password@hostIpOrName/mysql/DbName/charset" << std::endl
//                      << "for example: " << std::endl
//                      << "db://root:pass@10.20.30.253/mysql/tc-db-main/utf8" << std::endl;
//        }
//
//        std::cout << "Connector/C++ tutorial framework..." << std::endl;
//        std::cout << std::endl;
//
//        try {
//
//            /* INSERT TUTORIAL CODE HERE! */
//
//            for (int i = 1; i < argc; ++i) {
//                std::cout << "Begin work with " << argv[i] << std::endl;
//
//                std::cout << "try to clone connection " << argv[i] << std::endl;
//                Castle::MySQL::DbConnection connectionClone(argv[i]);
//                std::cout << "end to clone connection " << argv[i] << std::endl;
//                auto connection = connectionClone.clone();
//
//                auto statement = connection->createStatement();
//
//                std::cout << "using simple query " << std::endl;
//                Castle::MySQL::Ptr::ResultSetPtr result = Castle::MySQL::Ptr::WrapUniq(
//                        statement->executeQuery("select id,name from `tc-db-main`.devices"));
//
//                if (result->rowsCount() > 0) {
//                    std::cout << "ID   NAME " << std::endl;
//                    while (result->next()) {
//                        std::cout << result->getInt(1) << "  " << result->getString(2) << std::endl;
//                    }
//                }
//
//                std::cout << "using prepared query " << std::endl;
//                auto preparedStmt = connection->prepareStatement(
//                        "select id,name from `tc-db-main`.devices where id = (?) ");
//
//                preparedStmt->setInt(1, 2);
//                result = Castle::MySQL::Ptr::WrapUniq(preparedStmt->executeQuery());
//
//                if (result->rowsCount() > 0) {
//                    std::cout << "ID   NAME " << std::endl;
//                    while (result->next()) {
//                        std::cout << result->getInt(1) << "  " << result->getString(2) << std::endl;
//                    }
//                }
//                std::cout << "End work with " << argv[i] << std::endl;
//
//            }
//            /* INSERT TUTORIAL CODE HERE! */
//
//        } catch (sql::SQLException &e) {
//            /*
//              MySQL Connector/C++ throws three different exceptions:
//
//              - sql::MethodNotImplementedException (derived from sql::SQLException)
//              - sql::InvalidArgumentException (derived from sql::SQLException)
//              - sql::SQLException (derived from std::runtime_error)
//            */
//            std::cout << "# ERR: SQLException in " << __FILE__;
//            std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
//            /* what() (derived from std::runtime_error) fetches error message */
//            std::cout << "# ERR: " << e.what();
//            std::cout << " (MySQL error code: " << e.getErrorCode();
//            std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
//
//            return EXIT_FAILURE;
//        } catch (std::exception &e) {
//            std::cout << "Exception " << e.what() << std::endl;
//            return EXIT_FAILURE;
//        }
//
//        std::cout << "Done." << std::endl;
//    }
//
//    return EXIT_SUCCESS;
//}
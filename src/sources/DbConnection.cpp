//
// Created by iddqd13 on 3/13/18.
//

#include "DbConnection.h"

#include <cstdlib>
#include <sstream>
#include <regex>
#include <stdexcept>


#include <network/uri.hpp>

#include <boost/tokenizer.hpp>
#include <boost/algorithm/string/case_conv.hpp>

#include <mysql_driver.h>
#include <mysql_connection.h>

#include <cppconn/driver.h>


namespace Castle {

    namespace MySQL {

        DbConnection::DbConnection(const char *dbUri) : m_uri(dbUri) {
            try {
                network::uri dbInfo(dbUri);

                sql::ConnectOptionsMap connection_properties;

                if (dbInfo.has_host()) {
                    connection_properties["hostName"] = dbInfo.host().to_string();
                }
                if (dbInfo.has_port()) {
                    connection_properties["port"] = std::stoi(dbInfo.port().to_string());
                }

                if (dbInfo.has_user_info()) {
                    auto ui = dbInfo.user_info();
                    std::regex ex("([^:]*)(?::(.*))?");
                    std::cmatch what;
                    if (std::regex_match(ui.to_string().c_str(), what, ex)) {
                        connection_properties["userName"] = std::string(what[1].first, what[1].second);
                        connection_properties["password"] = what[2].length() ? std::string(what[2].first, what[2].second)
                                                                             : "";
                    }
                }
                if (dbInfo.has_path()) {
                    boost::char_separator<char> sep{"/"};
                    boost::tokenizer<boost::char_separator<char>> tok{dbInfo.path(), sep};
                    uint8_t pos = 0;

                    for (const auto &t : tok) {
                        switch (pos) {
                            case 0 : {
                                if (boost::to_lower_copy(t) != "mysql") {
                                    std::runtime_error("not supported DB type");
                                }
                            }
                                break;
                            case 1 :
                                connection_properties["schema"] = t;
                                break;  //get first entry.
                            case 2 :
                                connection_properties["OPT_CHARSET_NAME"] = t;
                                break;  //get first entry.
                            default:
                                break;
                        }
                        ++pos;
                    }
                }

                connection_properties["OPT_RECONNECT"] = true;

                auto *driver = sql::mysql::get_driver_instance();
                if (driver == nullptr) {
                    throw std::runtime_error("Error on getting mysql driver");
                }
                m_connection = Ptr::WrapUniq(driver->connect(connection_properties));

            } catch (std::exception &e) {
                throw e;
            }
        }

        Ptr::StatementPtr DbConnection::createStatement() {
            return Ptr::WrapUniq(m_connection->createStatement());
        }

        Ptr::PreparedStatementPtr DbConnection::prepareStatement(const sql::SQLString &sql) {
            return Ptr::WrapUniq(m_connection->prepareStatement(sql));
        }

        Ptr::DbConnectionPtr DbConnection::clone() const {
            return std::make_unique<DbConnection>(m_uri.c_str());
        }


        const char LOGS_INSERT_STATEMENT[] = "INSERT INTO `tc-db-log`.logs(LOGTIME,AREA,LOGDATA,EMPHINT,DEVHINT,FRAMETS) VALUES( (?), (?) , (?) , (?) ,(?) , (?) )";
        const char ALARMLOG_INSERT_STATEMENT[] = "INSERT INTO `tc-db-main`.alarmlog(`LOGTIME`,`LINEID`,`NEWSTATE`,`FRAMETS`) VALUES((?),(?),(?),(?));";

    }

}

